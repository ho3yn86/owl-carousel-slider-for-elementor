<?php

namespace WPC\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if (!defined('ABSPATH')) exit; // Exit if accessed directly


class owlorg extends Widget_Base{

  public function get_name(){
    return 'owlorg';
  }

  public function get_title(){
    return 'owlorg';
  }

  public function get_icon(){
    return 'eicon-text-field';
  }

  public function get_categories(){
    return ['general'];
  }

  protected function _register_controls(){

    $this->start_controls_section(
      'section_content',
      [
        'label' => 'Settings',
      ]
    );

    $this->add_control(
      'label_heading',
      [
        'label' => 'اسلاگ فیلد مورد نظر',
        'type' => \Elementor\Controls_Manager::TEXT,
        'default' => ''
      ]
    );

    $this->end_controls_section();
  }
  

  protected function render(){
    $settings = $this->get_settings_for_display();

    $this->add_inline_editing_attributes('label_heading', 'basic');
    $this->add_render_attribute(
      'label_heading',
      [
        'class' => ['field_by_slug_advertisement__label-heading'],
      ]
    );

    ?>
    <div class="advertisement">


















		<div id="qunit"></div>
		<div id="qunit-fixture">

			<div class="owl-carousel " id="demo" >
              <?php
              $my_custom_post_var_dump = get_post_meta( get_the_ID(),$settings['label_heading'])[0];
              foreach ($my_custom_post_var_dump as $value) {
              ?>

              <div class="item"><h4>
              <img src="<?php echo wp_get_attachment_image_src($value,'full')[0];  ?>" alt="Chania" width="460" height="245">
              </h4></div>


              <?php
              }
              ?>
		</div>

	<script>
		jQuery('.owl-carousel').owlCarousel({
    center: true,
    items:2,
    loop:true,
    margin:10,
    autoWidth:true,
    nav:true,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})

	</script>
    <style>
      :root {
      --myleft: 1px; 
      }

    .owl-carousel .owl-stage {
    position: relative;
    left: var(--myleft);
    }

    .owl-carousel .owl-item img {

    width: auto;
    max-height: 300px;
    height: 300px;
    }

    .owl-carousel .owl-stage-outer {
    height: 320px;
    background: #162719;
     }
     
     
     
     
     
     
     
     
        .owl-carousel .owl-stage-outer {
        border-left: 5px solid #162719;
        border-right: 5px solid #162719;
        border-radius: 7px;
          }
        .owl-dots{height:26px;}
        .owl-dots>button{outline:0px;}
         .owl-nav {display: inline-block;
        }
        .owl-carousel.owl-loaded.owl-drag>.owl-nav.owl-nav>button.owl-prev{position: absolute;top: 30%;right: 2%;border: #111 solid 0px;outline: #f00 auto 0px !important;}
        .owl-carousel.owl-loaded.owl-drag>.owl-nav.owl-nav>button.owl-next{position: absolute;top: 30%;left: 2%;border: #111 solid 0px;outline: #f00 auto 0px !important;}
        .owl-carousel.owl-loaded.owl-drag>.owl-nav.owl-nav>button.owl-prev>span{
            font-size: 0px;
        }
        .owl-carousel.owl-loaded.owl-drag>.owl-nav.owl-nav>button.owl-next>span{font-size: 0px;}
        .owl-dots{
            display: block;
            margin: auto;
            text-align: center;
        }
        .owl-carousel.owl-loaded.owl-drag>.owl-dots>button.owl-dot.active{background: #989696;width: 14px;height: 14px;margin: 0 1px 0 1px;transition: 0.4s;}
        .owl-carousel.owl-loaded.owl-drag>.owl-dots>button.owl-dot{background: #c5c5c5;width: 10px;height: 10px;margin: 0px 3px 0 3px;border-radius: 10px;}
        .owl-carousel.owl-loaded.owl-drag>.owl-nav.owl-nav>button.owl-prev>span::before{font-family: "Font Awesome 5 Free";font-weight: 400;content: "\f35a";color: #ffffff;/* background: #c2767600; */text-color: #c6acac;font-size: 40px;}
        .owl-carousel.owl-loaded.owl-drag>.owl-nav.owl-nav>button.owl-next>span::before{font-family: "Font Awesome 5 Free";font-weight: 400;content: "\f359";color: #f7f7f7;/* background: #c276769c; */text-color: #870b0b;font-size: 40px;/* filter: drop-shadow(2px 4px 0 #111); *//* background: #111111bd; */box-shadow: #111;/* text-shadow: 16px 7px #111; */}





  </style>

  <script>




  </script>




























    <?php
  }

  protected function _content_template(){
    ?>
    <#
        view.addInlineEditingAttributes( 'label_heading', 'basic' );
    view.addRenderAttribute(
        'label_heading',
        {
            'class': [ 'field_by_slug_advertisement__label-heading' ],
        }
    );
        #>
        <div class="field_by_slug_advertisement">
      <div {{{ view.getRenderAttributeString( 'label_heading' ) }}}>{{{ settings.label_heading }}}</div>
      <div class="field_by_slug_advertisement__content">
        <div class="field_by_slug_advertisement__content__heading">{{{ settings.content_heading }}}</div>
        <div class="field_by_slug_advertisement__content__copy">
          {{{ settings.content }}}
        </div>
      </div>
    </div>
        <?php
  }
}