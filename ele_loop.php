<?php
 /**
 * Plugin Name: my elementor custom loop
 * Plugin URI:  https://wordpress.org/
 * Description: Enables elementor custom loop
 * Version:     1.0.0
 * Author:      ho3yn
 * Author URI:  
 * Text Domain: ele-loop
 */
namespace WPC;

 include plugin_dir_path(__FILE__) . "functions.php";

    // use Elementor\Plugin;
    class Widget_Loader{
    
      private static $_instance = null;
    
      public static function instance()
      {
        if (is_null(self::$_instance)) {
          self::$_instance = new self();
        }
        return self::$_instance;
      }
    
    
      private function include_widgets_files(){
        //require_once(__DIR__ . '/widgets/advertisement.php');
        require_once(__DIR__ . '/widgets/owlorg.php');





      }
    
      public function register_widgets(){
    
        $this->include_widgets_files();
    
        //\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Widgets\Advertisement());

        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Widgets\owlorg());

       
      }
    
      public function __construct(){
        add_action('elementor/widgets/widgets_registered', [$this, 'register_widgets'], 99);
      }
    }
    
    // Instantiate Plugin Class
    Widget_Loader::instance();
 



    ?>
    
  
  
  
  
  
  
  
  
  
  
  
  